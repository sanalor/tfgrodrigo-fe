@test
Feature: Pruebas sobre la página web de la biblioteca de la Universidad de Valladolid

  Scenario Outline: Comprobar que un libro se encuentra en la Biblioteca
    When Yo estoy en la pagina web de la biblioteca de la Universidad de Valladolid
    And Yo escribo "<titulo>" en el catalogo de libros y le doy click en buscar
    Then Yo compruebo que obtengo "<titulo>" en la pagina de resultados
    And Yo compruebo que el libro esta disponible en cualquier Biblioteca
    Examples:
      | titulo     |
      | Cenicienta |