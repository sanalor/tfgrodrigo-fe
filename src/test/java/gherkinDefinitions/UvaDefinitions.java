package gherkinDefinitions;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import gherkinSteps.UvaSteps;
import net.thucydides.core.annotations.Steps;
import org.junit.Assert;

public class UvaDefinitions {
    @Steps
    private UvaSteps uvaSteps;

    @When("^Yo estoy en la pagina web de la biblioteca de la Universidad de Valladolid$")
    public void estoyEnLaPaginaDeLaBibliotecaDeLaUVA() {
        uvaSteps.navigateToUVALibraryWebPage();
    }

    @And("^Yo escribo \"([^\"]*)\" en el catalogo de libros y le doy click en buscar$")
    public void yoEscriboEnElCatalogoDeLibrosYLeDoyClickEnBuscar(String titulo) {
        uvaSteps.writeInSearchTabAndClickOnSearch(titulo);
    }

    @Then("^Yo compruebo que obtengo \"([^\"]*)\" en la pagina de resultados$")
    public void yoComprueboQueObtengoEnLaPaginaDeResultados(String titulo) {
        Assert.assertTrue("El título no esta entre los resultados", uvaSteps.searchTitlesInResultsAndValidate(titulo));
    }

    @And("^Yo compruebo que el libro esta disponible en cualquier Biblioteca$")
    public void yoComprueboQueElLibroEstaDisponibleEnCualquierBiblioteca() {
        Assert.assertTrue("El título no esta disponible para su alquiler", uvaSteps.checkAvailability());
    }
}
