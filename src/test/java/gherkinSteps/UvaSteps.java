package gherkinSteps;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import pageobjects.POUVALibrary;
import pageobjects.POUVALibraryResults;
import utilities.World;

import java.util.ArrayList;

public class UvaSteps {
    private POUVALibrary pouvaLibrary;
    private POUVALibraryResults pouvaLibraryResults;
    private World world = World.getWorld();
    private boolean containTitle = false;
    private boolean isAvailable = false;


    public void navigateToUVALibraryWebPage() {
        pouvaLibrary.openAndMaximize();
        pouvaLibrary.validateScreen();
    }

    public void writeInSearchTabAndClickOnSearch(String libro) {
        Assert.assertTrue("El elemento " + pouvaLibrary.getIframe() + " no aparece",pouvaLibrary.getIframe().isDisplayed());
        pouvaLibrary.getDriver().switchTo().frame(pouvaLibrary.getIframe());
        Assert.assertTrue("El elemento " + pouvaLibrary.getSearchTAB() + " no aparece", pouvaLibrary.getSearchTAB().isDisplayed());
        pouvaLibrary.getSearchTAB().click();
        pouvaLibrary.getSearchTAB().type(libro);
        Assert.assertTrue("El elemento " + pouvaLibrary.getSearchButton() + " no aparece", pouvaLibrary.getSearchButton().isDisplayed());
        pouvaLibrary.getSearchButton().waitUntilClickable().click();
    }

    public boolean searchTitlesInResultsAndValidate(String titulo) {
        ArrayList<String> tabs2 = new ArrayList<String> (pouvaLibraryResults.getDriver().getWindowHandles());
        pouvaLibraryResults.getDriver().switchTo().window(tabs2.get(1));
        pouvaLibraryResults.getWaiter().waitUntilVisible();
        for(WebElement libro : pouvaLibraryResults.getLibros()){
            containTitle = libro.getText().contains(titulo);
            if(containTitle = true){ return containTitle;}
        }
        return containTitle;
    }

    public boolean checkAvailability() {
        for(WebElement disponibilidad : pouvaLibraryResults.getDisponibilidad()){
            isAvailable = disponibilidad.getText().contains("Disponible");
            if(isAvailable = true){ return isAvailable;}
        }
        return isAvailable;
    }
}

