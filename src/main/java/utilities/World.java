package utilities;
import io.restassured.response.Response;
import net.serenitybdd.core.Serenity;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.util.Properties;

public class World {

    private Properties properties = new Properties();
    private Response response;
    private static final Logger LOGGER = Logger.getLogger(World.class);


    private static final String SESSION_VARIABLE_NAME = "World";


    public static World getWorld() {
        if (Serenity.hasASessionVariableCalled(SESSION_VARIABLE_NAME)) {
            return Serenity.sessionVariableCalled(SESSION_VARIABLE_NAME);
        } else {
            World world = new World();
            Serenity.setSessionVariable(SESSION_VARIABLE_NAME).to(world);
            return world;
        }
    }

    public static World clearWorld() {
        World world = new World();
        Serenity.setSessionVariable(SESSION_VARIABLE_NAME).to(world);
        return world;
    }

    public Response getResponse() {
        return response;
    }

    public void setResponse(Response response) {
        this.response = response;
    }

    public Properties getProperties() {
        return properties;
    }

    public void setProperties(Properties properties) {
        this.properties = properties;
    }

    /**
     * Read file properties
     *
     * @param path Path where is properties file
     */
    public void readPropertiesFileToWorld(String path) {
        try {
            this.getProperties().load(World.class.getClassLoader().getResourceAsStream(path));
        } catch (NullPointerException e) {
            LOGGER.info("Unable to access properties file: " + path + ". Exception " + e);
        } catch (IOException e) {
            LOGGER.info("Found the properties file but could access it. Exception " + e);
        }
    }
}