package pageobjects;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import org.junit.Assert;
import org.openqa.selenium.WebElement;
import utilities.World;

public class POUVALibrary extends PageObject {
    @FindBy(css = "a[href='http://www.uva.es/index.html']")
    private WebElementFacade identifierUVA;

    @FindBy(css = "input[id='textoAbuscarCatalogos']")
    private WebElementFacade searchTAB;

    @FindBy(css = "input[id='boton_buscar_catalogo']")
    private WebElementFacade searchButton;

    @FindBy(css = "iframe[class='cuadrobuscar']")
    private WebElement iframe;


    public void openAndMaximize() {
        this.setDefaultBaseUrl(World.getWorld().getProperties().get("url_uva_library").toString());
        this.getDriver().manage().window().maximize();
        open();
    }

    public void validateScreen() {
        Assert.assertTrue("Element " + identifierUVA + " does not appear", identifierUVA.isDisplayed());
    }

    public WebElementFacade getSearchTAB() {
        return searchTAB;
    }

    public WebElementFacade getSearchButton() {
        return searchButton;
    }

    public WebElement getIframe() {
        return iframe;
    }

}
