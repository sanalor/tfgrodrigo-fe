package pageobjects;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import org.openqa.selenium.WebElement;

import java.util.List;

public class POUVALibraryResults extends PageObject {
    @FindBy(css = "span[dir='auto']")
    List<WebElement> libros;

    @FindBy(css = "div[class='_md-container md-ink-ripple']")
    WebElementFacade waiter;

    @FindBy(css = "span[translate='delivery.code.available_in_library']")
    List<WebElement> disponibilidad;

    public List<WebElement> getLibros() {
        return libros;
    }

    public List<WebElement> getDisponibilidad() {
        return disponibilidad;
    }

    public WebElementFacade getWaiter() {
        return waiter;
    }
}
